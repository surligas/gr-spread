/* -*- c++ -*- */
/*
 * gr-spread: Spread Spectrum research and experimentation
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INCLUDED_SPREAD_SPREADING_SEQUENCE_H
#define INCLUDED_SPREAD_SPREADING_SEQUENCE_H

#include <spread/api.h>
#include <gnuradio/digital/lfsr.h>

#include <cstdint>
#include <cstddef>
#include <vector>
#include <boost/shared_ptr.hpp>

namespace gr {
namespace spread {

/*!
 * \brief Spreading sequence generator
 *
 */
class SPREAD_API spreading_sequence {
public:
  typedef boost::shared_ptr<spreading_sequence> spreading_sequence_sptr;

  static spreading_sequence_sptr
  make(uint32_t polya_mask, uint32_t polya_seed,
       size_t polya_order, uint32_t polyb_mask,
       uint32_t polyb_seed, size_t polyb_order);

  static spreading_sequence_sptr
  make(uint32_t polya_mask, uint32_t polya_seed,
       size_t polya_order, uint32_t polyb_mask,
       uint32_t polyb_seed, size_t polyb_order,
       uint32_t polyc_mask, uint32_t polyc_seed,
       size_t polyc_order);

  spreading_sequence(uint32_t polya_mask, uint32_t polya_seed,
                     size_t polya_order, uint32_t polyb_mask,
                     uint32_t polyb_seed, size_t polyb_order);

  spreading_sequence(uint32_t polya_mask, uint32_t polya_seed,
                     size_t polya_order, uint32_t polyb_mask,
                     uint32_t polyb_seed, size_t polyb_order,
                     uint32_t polyc_mask, uint32_t polyc_seed,
                     size_t polyc_order);
  ~spreading_sequence();

  void
  reset();

  std::string
  to_string();

  std::vector<uint8_t>
  to_vector();

  uint8_t
  next_bit();

  size_t
  length() const;
private:
  const size_t d_order;
  const bool d_2channels;
  digital::lfsr d_a;
  digital::lfsr d_b;
  digital::lfsr d_c;
  size_t d_idx;
};

} // namespace spread
} // namespace gr

#endif /* INCLUDED_SPREAD_SPREADING_SEQUENCE_H */

