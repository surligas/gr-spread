/* -*- c++ -*- */
/*
 * gr-spread: Spread Spectrum research and experimentation
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INCLUDED_SPREAD_SPREADING_PD_H
#define INCLUDED_SPREAD_SPREADING_PD_H

#include <spread/api.h>
#include <gnuradio/sync_block.h>
#include <spread/spreading_sequence.h>

namespace gr {
namespace spread {

/*!
 * \brief PDU spreading to a tagged stream block
 * \ingroup spread
 *
 */
class SPREAD_API spreading_pd : virtual public gr::sync_block {
public:
  typedef boost::shared_ptr<spreading_pd> sptr;

  /*!
   * \brief Return a shared_ptr to a new instance of spread::spreading_pd.
   *
   * To avoid accidental use of raw pointers, spread::spreading_pd's
   * constructor is in a private implementation
   * class. spread::spreading_pd::make is the public interface for
   * creating new instances.
   */
  static sptr
  make(spreading_sequence::spreading_sequence_sptr seq,
       size_t repeat,
       const std::string &len_tag_name);
};

} // namespace spread
} // namespace gr

#endif /* INCLUDED_SPREAD_SPREADING_PD_H */

