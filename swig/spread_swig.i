/* -*- c++ -*- */

#define SPREAD_API

%include "gnuradio.i"           // the common stuff

//load generated python docstrings
%include "spread_swig_doc.i"

%template(spreading_sequence_sptr) boost::shared_ptr<gr::spread::spreading_sequence>;

%{
#include "spread/spreading_sequence.h"
#include "spread/spreading_pd.h"
%}

%include "spread/spreading_sequence.h"

%include "spread/spreading_pd.h"
GR_SWIG_BLOCK_MAGIC2(spread, spreading_pd);
