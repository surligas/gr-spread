/* -*- c++ -*- */
/*
 * gr-spread: Spread Spectrum research and experimentation
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INCLUDED_SPREAD_SPREADING_PD_IMPL_H
#define INCLUDED_SPREAD_SPREADING_PD_IMPL_H

#include <spread/spreading_pd.h>
#include <vector>
namespace gr {
namespace spread {

class spreading_pd_impl : public spreading_pd {

public:
  spreading_pd_impl(spreading_sequence::spreading_sequence_sptr seq,
                    size_t repeat,
                    const std::string &len_tag_name);
  ~spreading_pd_impl();

  // Where all the action really happens
  int
  work(int noutput_items, gr_vector_const_void_star &input_items,
       gr_vector_void_star &output_items);

private:
  spreading_sequence::spreading_sequence_sptr d_seq;
  const size_t d_repeat;
  const std::string d_len_tag_name;
  bool d_pdu_sent;
  size_t d_bit_idx;
  size_t d_chip_idx;
  pmt::pmt_t d_port;
  pmt::pmt_t d_len_tag;
  size_t d_pdu_len;
  size_t d_repeat_cnt;
  std::vector<uint8_t> d_pdu;
};

} // namespace spread
} // namespace gr

#endif /* INCLUDED_SPREAD_SPREADING_PD_IMPL_H */

