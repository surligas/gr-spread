/* -*- c++ -*- */
/*
 * gr-spread: Spread Spectrum research and experimentation
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "spreading_pd_impl.h"

namespace gr {
namespace spread {

spreading_pd::sptr
spreading_pd::make(spreading_sequence::spreading_sequence_sptr seq,
                   size_t repeat,
                   const std::string &len_tag_name)
{
  return gnuradio::get_initial_sptr(new spreading_pd_impl(seq, repeat,
                                    len_tag_name));
}

/*
 * The private constructor
 */
spreading_pd_impl::spreading_pd_impl(spreading_sequence::spreading_sequence_sptr
                                     seq,
                                     size_t repeat,
                                     const std::string &len_tag_name)
  : gr::sync_block("spreading_pd",
                   gr::io_signature::make(0, 0, 0),
                   gr::io_signature::make(1, 1, sizeof(uint8_t))),
    d_seq(seq),
    d_repeat(repeat),
    d_len_tag_name(len_tag_name),
    d_pdu_sent(true),
    d_bit_idx(0),
    d_chip_idx(0),
    d_port(pmt::mp("in")),
    d_len_tag(pmt::mp(len_tag_name)),
    d_pdu_len(0),
    d_repeat_cnt(0)
{
  message_port_register_in(d_port);
}

/*
 * Our virtual destructor.
 */
spreading_pd_impl::~spreading_pd_impl()
{
}

int
spreading_pd_impl::work(int noutput_items,
                        gr_vector_const_void_star &input_items,
                        gr_vector_void_star &output_items)
{
  uint8_t *out = (uint8_t *) output_items[0];
  if (d_pdu_sent) {
    pmt::pmt_t msg(delete_head_nowait(d_port));
    if (msg.get() == NULL) {
      return 0;
    }
    pmt::pmt_t blob(pmt::cdr(msg));
    if (!pmt::is_blob(blob)) {
      throw std::runtime_error("Spreading: PMT must be blob");
    }

    d_pdu_len = pmt::blob_length(blob);
    d_pdu.clear();
    d_pdu = std::vector<uint8_t>(d_pdu_len);
    memcpy(&d_pdu[0], (const uint8_t *)pmt::blob_data(blob), d_pdu_len);
    d_pdu_sent = false;
    d_bit_idx = 0;
    d_chip_idx = 0;
    d_seq->reset();
    add_item_tag(0, nitems_written(0), d_len_tag,
                 pmt::from_long(d_repeat * d_pdu_len * 8 * d_seq->length()),
                 alias_pmt());
  }

  for (size_t i = 0; i < (size_t)noutput_items; i++) {
    uint8_t b = (d_pdu[d_bit_idx / 8] >> (7 - (d_bit_idx % 8))) & 0x1;
    out[i] = b ^ d_seq->next_bit();
    d_chip_idx++;
    if (d_chip_idx == d_seq->length()) {
      d_repeat_cnt++;
      d_chip_idx = 0;
      d_seq->reset();

      if (d_repeat_cnt == d_repeat) {
        d_bit_idx++;
        d_repeat_cnt = 0;

        if (d_bit_idx == d_pdu_len * 8) {
          d_pdu_sent = true;
          return i + 1;
        }
      }
    }
  }
  return noutput_items;
}

} /* namespace spread */
} /* namespace gr */

