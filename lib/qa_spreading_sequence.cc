/* -*- c++ -*- */
/*
 * gr-spread: Spread Spectrum research and experimentation
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <spread/spreading_sequence.h>
#include <gnuradio/attributes.h>
#include <boost/test/unit_test.hpp>

namespace gr {
namespace spread {

BOOST_AUTO_TEST_CASE(test_spreading_sequence_t1)
{
  size_t order = 10;
  spreading_sequence x(0x33, 0x3140, order, 0x57, 0x120, order);
  BOOST_REQUIRE((1UL << (order + 1)) - 1 == x.length());
  BOOST_REQUIRE(x.to_string().length() == x.length());
}

BOOST_AUTO_TEST_CASE(test_spreading_sequence_t2)
{
  size_t order = 10;
  spreading_sequence x1(0x33, 0x3140, order, 0x57, 0x120, order);
  spreading_sequence x2(0x33, 0x3140, order, 0x57, 0x120, order);
  for (size_t i = 0; i < x1.length(); i++) {
    BOOST_REQUIRE(x1.next_bit() == x2.next_bit());
  }
}

BOOST_AUTO_TEST_CASE(test_spreading_sequence_t3)
{
  size_t order = 10;
  spreading_sequence x1(0x33, 0x3140, order, 0x57, 0x120, order);
  BOOST_REQUIRE(x1.to_vector().size() == x1.length());
}


} /* namespace spread */
} /* namespace gr */
